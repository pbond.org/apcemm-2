#!/bin/bash

# Depencies believed to be comprehensive as of 2023-02-22


# update system

sudo apt -y update
sudo apt -y upgrade


# install CMAKE

##sudo apt -y remove cmake
sudo apt -y install cmake


# install NETCDF

##sudo apt -y remove netcdf-bin libnetcdf-dev
sudo apt -y install netcdf-bin libnetcdf-dev

# install NETCDF C++
sudo apt -y install libnetcdf-c++4-dev

echo -e "\n\nNETCDF LOCATION CHECKS:\n"
sudo nc-config --libdir --includedir
sudo ncxx4-config --libdir --includedir


# install FFTW3

sudo apt -y install libfftw3-bin
sudo apt -y install libfftw3-dev


# install BOOST
sudo apt -y install libboost-all-dev


# install yaml-cpp

sudo apt -y install libyaml-cpp-dev


# list versions

echo -e "\n\nVERSION CHECKS:\n"
cmake --version
nc-config --version
ncxx4-config --version


# *END*
